import { render, screen } from "@testing-library/react";
import { Provider } from "react-redux";
import { store, persistor } from "../app/store";
import { PersistGate } from "redux-persist/integration/react";
import { BrowserRouter } from "react-router-dom";
import Footer from "../components/Footer";

test("should render text", () => {
  render(
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <BrowserRouter>
          <Footer />
        </BrowserRouter>
      </PersistGate>
    </Provider>
  );
  expect(
    screen.getByText(/Tempat jual-beli pakaian bekas dengan mutu berkualitas/i)
  ).toBeInTheDocument();
});
