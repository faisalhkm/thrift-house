import { render, screen, fireEvent } from "@testing-library/react";
import { Provider } from "react-redux";
import { store, persistor } from "../app/store";
import { PersistGate } from "redux-persist/integration/react";
import { BrowserRouter } from "react-router-dom";
import Checkout from "../pages/Checkout";

test("should render text", () => {
  render(
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <BrowserRouter>
          <Checkout />
        </BrowserRouter>
      </PersistGate>
    </Provider>
  );
  expect(screen.getByText(/Ringkasan belanja/i)).toBeInTheDocument();
});

test("should open modal address", () => {
  render(
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <BrowserRouter>
          <Checkout />
        </BrowserRouter>
      </PersistGate>
    </Provider>
  );

  fireEvent.click(screen.getByText(/Pilih Alamat Lain/i));
  expect(screen.getByText(/Pilih Alamat Pengiriman/i)).toBeInTheDocument();
});

test("should open address modal", () => {
  render(
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <BrowserRouter>
          <Checkout />
        </BrowserRouter>
      </PersistGate>
    </Provider>
  );
});
