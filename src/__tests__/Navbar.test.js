import { render, screen, fireEvent } from "@testing-library/react";
import { Provider } from "react-redux";
import { store, persistor } from "../app/store";
import { PersistGate } from "redux-persist/integration/react";
import { BrowserRouter } from "react-router-dom";
import Navbar from "../components/Navbar";

test("should render text", () => {
  render(
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <BrowserRouter>
          <Navbar />
        </BrowserRouter>
      </PersistGate>
    </Provider>
  );
  expect(screen.getByText(/Jualan di ThriftHouse/i)).toBeInTheDocument();
  expect(screen.getAllByText(/Pria/i)[0]).toBeInTheDocument();
  expect(screen.getAllByText(/Wanita/i)[0]).toBeInTheDocument();
  expect(screen.getAllByText(/Anak-anak/i)[0]).toBeInTheDocument();
});

test("should open dropdown", () => {
  render(
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <BrowserRouter>
          <Navbar />
        </BrowserRouter>
      </PersistGate>
    </Provider>
  );

  fireEvent.click(screen.getAllByText(/Pria/i)[0]);
  expect(screen.getByText(/jeans/i)).toBeVisible();
});
