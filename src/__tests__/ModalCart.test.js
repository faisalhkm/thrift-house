import { render, screen } from "@testing-library/react";
import { Provider } from "react-redux";
import { store, persistor } from "../app/store";
import { PersistGate } from "redux-persist/integration/react";
import { BrowserRouter } from "react-router-dom";
import ModalCart from "../components/ModalCart";

test("should render text", () => {
  render(
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <BrowserRouter>
          <ModalCart />
        </BrowserRouter>
      </PersistGate>
    </Provider>
  );
  expect(screen.getByText(/Lanjutkan Berbelanja/i)).toBeInTheDocument();
  expect(screen.getByText(/Produk Berhasil Ditambahkan!/i)).toBeInTheDocument();
});
