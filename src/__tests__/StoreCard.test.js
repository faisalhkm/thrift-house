import { render, screen } from "@testing-library/react";
import { Provider } from "react-redux";
import { store, persistor } from "../app/store";
import { PersistGate } from "redux-persist/integration/react";
import { BrowserRouter } from "react-router-dom";
import StoreCard from "../components/StoreCard";

test("should render text", () => {
  render(
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <BrowserRouter>
          <StoreCard oneStore={{ photo: "", name: "Apple" }} />
        </BrowserRouter>
      </PersistGate>
    </Provider>
  );
  expect(screen.getByText(/Apple/i)).toBeInTheDocument();
});
