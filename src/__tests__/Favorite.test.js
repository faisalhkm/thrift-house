import { render, screen } from "@testing-library/react";
import { Provider } from "react-redux";
import { store, persistor } from "../app/store";
import { PersistGate } from "redux-persist/integration/react";
import { BrowserRouter } from "react-router-dom";
import Favorite from "../pages/Favorite";

test("should render text", () => {
  render(
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <BrowserRouter>
          <Favorite />
        </BrowserRouter>
      </PersistGate>
    </Provider>
  );
  expect(screen.getByText(/Produk Favorit/i)).toBeInTheDocument();
  expect(screen.getByText(/Terakhir Disimpan/i)).toBeInTheDocument();
  expect(
    screen.getByPlaceholderText(/Cari produk favoritmu/i)
  ).toBeInTheDocument();
});
