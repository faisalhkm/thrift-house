import { render, screen, fireEvent } from "@testing-library/react";
import { Provider } from "react-redux";
import { store, persistor } from "../app/store";
import { PersistGate } from "redux-persist/integration/react";
import { BrowserRouter } from "react-router-dom";
import CheckoutProduct from "../components/checkout/CheckoutProduct";

test("should render text", () => {
  render(
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <BrowserRouter>
          <CheckoutProduct
            item={{
              store: {},
              product: [{ name: "Nike", photos: "", price: 12000 }],
            }}
            selectedAlamat={{ idCity: 1 }}
            setTransactions={() => null}
          />
        </BrowserRouter>
      </PersistGate>
    </Provider>
  );
  expect(screen.getByText(/Metode pengiriman/i)).toBeInTheDocument();
});

test("should open service modal", () => {
  render(
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <BrowserRouter>
          <CheckoutProduct
            item={{
              store: {},
              product: [{ name: "Nike", photos: "", price: 12000 }],
            }}
            selectedAlamat={{ idCity: 1 }}
            setTransactions={() => null}
          />
        </BrowserRouter>
      </PersistGate>
    </Provider>
  );
});
