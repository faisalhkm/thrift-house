import { render, screen } from "@testing-library/react";
import { Provider } from "react-redux";
import { store, persistor } from "../app/store";
import { PersistGate } from "redux-persist/integration/react";
import { BrowserRouter } from "react-router-dom";
import ProductCart from "../components/ProductCard";

test("should render text", () => {
  render(
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <BrowserRouter>
          <ProductCart
            id={123}
            key={123}
            name={"To the moon"}
            brand={"Nike"}
            size={42}
            condition={"bekas"}
            price={100000}
            photos={""}
            category={"Pria"}
          />
        </BrowserRouter>
      </PersistGate>
    </Provider>
  );
  expect(screen.getByText(/Nike/i)).toBeInTheDocument();
});
