const initialState = { value: {} };

const alamatReducer = (state = initialState, action) => {
  switch (action.type) {
    case "alamat/edit":
      return { ...state, value: action.payload };
    case "alamat/delete":
      return { ...state, value: {} };
    default:
      return state;
  }
};

export default alamatReducer;
