const initialState = {
    modalPhoto:false,
    url:""
}

const ModalPotoReducer = (state = initialState, action) => {
    switch(action.type){
        case 'IS-DISPLAY-MODALPOTO':
            return {
                    ...state,
                    modalPhoto:action.payload.dislay,
                    url:action.payload.url
                }
        default:
            return state;
    }
}

export default ModalPotoReducer;