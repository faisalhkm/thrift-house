const initialState = {
    isDisplayModal:false,
    idOrderUlasan:"",
    idStore:""

}

const modalUlasanReducer = (state = initialState, action) => {
    switch(action.type){
        case 'set-modal-ulasan':
            return {
                    ...state,
                    isDisplayModal:action.payload.modal,
                    idOrderUlasan:action.payload.id,
                    idStore:action.payload.idToko

                }
        default:
            return state;
    }
}

export default modalUlasanReducer;