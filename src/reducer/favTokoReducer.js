const initialState = { value: [] };

const favTokoReducer = (state = initialState, action) => {
  switch (action.type) {
    case "favtoko/get":
      return { ...state, value: action.payload };
    case "favtoko/add":
      if (state.value.includes(action.payload)) {
        const newArr = state.value.filter((e) => e !== action.payload);
        return { ...state, value: newArr };
      }
      return { ...state, value: [...state.value, action.payload] };
    default:
      return state;
  }
};

export default favTokoReducer;
