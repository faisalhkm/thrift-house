export const accordionDataPria = [
  {
    title: "Atasan",
    content: [
      { to: "/pria", text: "Kaos Lengan pendek", params: "kaoslenganpendek" },
      { to: "/pria", text: "Kaos Lengan panjang", params: "kaoslenganpanjang" },
      { to: "/pria", text: "Kemeja", params: "kemeja" },
      { to: "/pria", text: "Polo", params: "polo" },
    ],
  },
  {
    title: "Bawahan",
    content: [
      { to: "/pria", text: "Celana pendek", params: "celanapendek" },
      { to: "/pria", text: "Celana panjang", params: "celanapanjang" },
      { to: "/pria", text: "Casual", params: "casual" },
      { to: "/pria", text: "Jeans", params: "jeans" },
    ],
  },
  {
    title: "Luaran",
    content: [
      { to: "/pria", text: "Crewneck", params: "crewneck" },
      { to: "/pria", text: "Celana panjang", params: "celanapanjang" },
      { to: "/pria", text: "Hoodie", params: "hoodie" },
      { to: "/pria", text: "Jaket", params: "jaket" },
      { to: "/pria", text: "Jas", params: "jas" },
      { to: "/pria", text: "Mantel", params: "mantel" },
      { to: "/pria", text: "Sweater", params: "sweater" },
    ],
  },
  {
    title: "Sepatu",
    content: [
      { to: "/pria", text: "Boots", params: "boots" },
      { to: "/pria", text: "Formal", params: "formal" },
      { to: "/pria", text: "Sendal", params: "sendal" },
      { to: "/pria", text: "Slip on", params: "slipon" },
      { to: "/pria", text: "Sneakers", params: "sneakers" },
      { to: "/pria", text: "Olahraga", params: "olahraga" },
    ],
  },
  {
    title: "Aksesoris",
    content: [
      { to: "/pria", text: "Ikat Pinggang", params: "ikatpinggang" },
      { to: "/pria", text: "Jam", params: "jam" },
      { to: "/pria", text: "Kacamata", params: "kacamata" },
      { to: "/pria", text: "Topi", params: "topi" },
    ],
  },
];

export const accordionDataWanita = [
  {
    title: "Atasan",
    content: [
      { to: "/wanita", text: "Kaos Lengan pendek & 3/4", params: "kaoslenganpendek&3/4" },
      { to: "/wanita", text: "Kaos Lengan panjang", params: "kaoslenganpanjang" },
      { to: "/wanita", text: "Kemeja & Blus", params: "kemeja&blus" },
      { to: "/wanita", text: "Gaun", params: "gaun" },
    ],
  },
  {
    title: "Bawahan",
    content: [
      { to: "/wanita", text: "Celana pendek", params: "celanapendek" },
      { to: "/wanita", text: "Celana panjang", params: "celanapanjang" },
      { to: "/wanita", text: "Rok pendek & 3/4", params: "rokpendek&3/4" },
      { to: "/wanita", text: "Rok panjang", params: "rokpanjang" },
    ],
  },
  {
    title: "Luaran",
    content: [
      { to: "/wanita", text: "Cardigan", params: "cardigan" },
      { to: "/wanita", text: "Crewneck", params: "crewneck" },
      { to: "/wanita", text: "Hoodie", params: "hoodie" },
      { to: "/wanita", text: "Jaket", params: "jaket" },
      { to: "/wanita", text: "Jas", params: "jas" },
      { to: "/wanita", text: "Mantel", params: "mantel" },
      { to: "/wanita", text: "Sweater", params: "sweater" },
    ],
  },
  {
    title: "Sepatu",
    content: [
      { to: "/wanita", text: "Boots", params: "boots" },
      { to: "/wanita", text: "Heels & Wedges", params: "heels&wedges" },
      { to: "/wanita", text: "Sendal", params: "sendal" },
      { to: "/wanita", text: "Slip on", params: "slipon" },
      { to: "/wanita", text: "Sneakers", params: "sneakers" },
      { to: "/wanita", text: "Olahraga", params: "ola" },
    ],
  },
  {
    title: "Aksesoris",
    content: [
      { to: "/wanita", text: "Ikat Pinggang", params: "ikatpinggang" },
      { to: "/wanita", text: "Jam", params: "jam" },
      { to: "/wanita", text: "Kacamata", params: "kacamata" },
      { to: "/wanita", text: "Topi", params: "topi" },
    ],
  },
];

export const accordionDataAnak = [
  {
    title: "Atasan",
    content: [
      { to: "/anak", text: "Kaos Lengan pendek", params: "kaoslenganpendek" },
      { to: "/anak", text: "Kaos Lengan panjang", params: "kaoslenganpanjang" },
      { to: "/anak", text: "Kemeja & Blus", params: "kemeja&blus" },
      { to: "/anak", text: "Gaun", params: "gaun" },
      { to: "/anak", text: "Polo", params: "polo" },
    ],
  },
  {
    title: "Bawahan",
    content: [
      { to: "/anak", text: "Celana pendek" , params: "celanapendek"},
      { to: "/anak", text: "Celana panjang", params: "celanapanjang" },
      { to: "/anak", text: "Rok pendek & 3/4", params: "rokpendek&3/4" },
      { to: "/anak", text: "Rok panjang", params: "rokpanjang" },
    ],
  },
  {
    title: "Luaran",
    content: [
      { to: "/anak", text: "Cardigan", params: "cardigan" },
      { to: "/anak", text: "Crewneck", params: "crewneck" },
      { to: "/anak", text: "Hoodie", params: "hoodie" },
      { to: "/anak", text: "Jaket", params: "jaket" },
      { to: "/anak", text: "Jas", params: "jas" },
      { to: "/anak", text: "Mantel", params: "mantel" },
      { to: "/anak", text: "Sweater", params: "sweater" },
    ],
  },
  {
    title: "Sepatu",
    content: [
      { to: "/anak", text: "Boots", params: "boots" },
      { to: "/anak", text: "Formal", params: "formal" },
      { to: "/anak", text: "Heels & Wedges", params: "heels&wedges" },
      { to: "/anak", text: "Sendal", params: "sendal" },
      { to: "/anak", text: "Slip on", params: "slipon" },
      { to: "/anak", text: "Sneakers", params: "sneakers" },
      { to: "/anak", text: "Olahraga", params: "olahraga" },
    ],
  },
  {
    title: "Aksesoris",
    content: [
      { to: "/anak", text: "Kacamata", params: "kacamata" },
      { to: "/anak", text: "Topi", params: "topi" },
    ],
  },
];
