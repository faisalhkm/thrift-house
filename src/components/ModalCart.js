export default function ModalCart(props) {
  return (
    <div onClick={props.onCancel} className="bg-black-rgba fixed flex z-50 top-0 bottom-0 left-0 right-0">
      <div onClick={(e) => e.stopPropagation()} className="bg-white rounded-lg m-auto text-center overflow-hidden">
        <div className=" px-14 pt-7 pb-6">
          <p className="font-bold text-sm mb-2">Produk Berhasil Ditambahkan!</p>
        </div>
        <div className="flex">
          <button type="button" onClick={props.onCancel} className="flex-grow py-4 text-xs border-t basis-1/2 hover:bg-[#eeeeee]">
            Lanjutkan Berbelanja
          </button>
          <button onClick={props.onAccept} className="flex-grow py-4 text-xs border-t border-t-[#29A867] text-white bg-[#29A867] hover:bg-[#4DB680] basis-1/2">
            Ke Keranjang
          </button>
        </div>
      </div>
    </div>
  );
}
