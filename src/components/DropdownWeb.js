import { Link, createSearchParams } from "react-router-dom";
import {
  accordionDataPria,
  accordionDataWanita,
  accordionDataAnak,
} from "../utils/accordionData";

const DropdownWeb = ({ isDropdownOpen, setIsDropdownOpen }) => {
  const closeDropdown = () => setIsDropdownOpen("");

  return (
    <>
      <div className="relative customcontainer mx-auto">
        {/* pria */}
        <div
          className={`dropdownContent customcontainer mx-auto py-7 px-6 space-x-16 absolute z-20 bg-white w-full ${
            isDropdownOpen === "Pria" ? "flex" : "hidden"
          }`}
        >
          {accordionDataPria.map((el, index) => (
            <div key={index} className="flex flex-col">
              <h4 className="font-semibold text-xl mb-2">{el.title}</h4>
              {el.content.map((val, key) => (
                <span key={key}>
                  <Link
                    to={
                      val.to +
                      "?" +
                      createSearchParams({
                        subcategory1: el.title?.toLowerCase(),
                        subcategory2: val.params?.toLowerCase(),
                      }).toString()
                    }
                    onClick={closeDropdown}
                    className="hover:text-gogreen"
                  >
                    {val.text}
                  </Link>
                </span>
              ))}
            </div>
          ))}

          <div>
            <img src="/images/navbar-pria.png" alt="navbar-pria" />
          </div>
        </div>
      </div>

      <div className="relative customcontainer mx-auto">
        {/* wanita */}
        <div
          className={`dropdownContent customcontainer mx-auto py-7 px-6 space-x-16 absolute z-20 bg-white w-full ${
            isDropdownOpen === "Wanita" ? "flex" : "hidden"
          }`}
        >
          {accordionDataWanita.map((el, index) => (
            <div key={index} className="flex flex-col">
              <h4 className="font-semibold text-xl mb-2">{el.title}</h4>
              {el.content.map((val, key) => (
                <span key={key}>
                  <Link
                    to={
                      val.to +
                      "?" +
                      createSearchParams({
                        subcategory1: el.title?.toLowerCase(),
                        subcategory2: val.params?.toLowerCase(),
                      }).toString()
                    }
                    onClick={closeDropdown}
                    className="hover:text-gogreen"
                  >
                    {val.text}
                  </Link>
                </span>
              ))}{" "}
            </div>
          ))}

          <div>
            <img src="/images/navbar-wanita.png" alt="navbar-pria" />
          </div>
        </div>
      </div>

      <div className="relative customcontainer mx-auto">
        {/* anak */}
        <div
          className={`dropdownContent customcontainer mx-auto py-7 px-6 space-x-16 absolute z-20 bg-white w-full ${
            isDropdownOpen === "Anak-anak" ? "flex" : "hidden"
          }`}
        >
          {accordionDataAnak.map((el, index) => (
            <div key={index} className="flex flex-col">
              <h4 className="font-semibold text-xl mb-2">{el.title}</h4>
              {el.content.map((val, key) => (
                <span key={key}>
                  <Link
                    to={
                      val.to +
                      "?" +
                      createSearchParams({
                        subcategory1: el.title?.toLowerCase(),
                        subcategory2: val.params?.toLowerCase(),
                      }).toString()
                    }
                    onClick={closeDropdown}
                    className="hover:text-gogreen"
                  >
                    {val.text}
                  </Link>
                </span>
              ))}{" "}
            </div>
          ))}

          <div>
            <img src="/images/navbar-anak.png" alt="navbar-pria" />
          </div>
        </div>
      </div>
    </>
  );
};

export default DropdownWeb;
