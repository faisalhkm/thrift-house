import axios from "axios";
import React, { Fragment, useEffect, useState } from "react";
import { useSelector } from "react-redux";
import {useParams } from "react-router-dom";
import { favTokoAdd, isModalLoginFunc } from "../../action";
import TokoDesc from "./TokoDesc";
import { useDispatch} from "react-redux";


export default function TokoDescWrap(){
    const params =useParams()
    const[oneStore, setOneStore] = useState([])
    const [isLoading, setIsLoading] = useState(true);
    const [isLoading2, setIsLoading2] = useState(false);
    // const [isLoading1, setIsLoading1] = useState(true);
    const { access_token: accessToken } = useSelector((state) => state.login);
    const dispatch = useDispatch()
    const favFlag = useSelector((state) => state.favToko.value);
    const [isFavClicked, setIsFavClicked] = useState(false);
    const { id: userId } = useSelector((state) => state.login);

  useEffect(() => {
    setIsFavClicked(favFlag.includes(params.id));
    axios.get(`https://thrifthouse.herokuapp.com:443/api/v1/stores/${params.id}/informations`)
        .then((respone) => {
            setOneStore(respone.data.data)
            setIsLoading(false)
        })
        .catch((error) => {
            console.log(error)
        })
  }, [favFlag, params.id]);

  const favHandler = () => {
    setIsLoading2(true)
      axios
        .post(
          `https://thrifthouse.herokuapp.com:443/api/v1/users/${userId}/stores/favorites`,
          { storeId: params.id }
        )
        .then((res) => {
          dispatch(favTokoAdd(res.data.data.storeId));
          setIsFavClicked((prev) => !prev);
          setIsLoading2(false)
        })
        .catch((err) => {console.log(err);setIsLoading2(false)});
  };

    function showHiddenModal(e){
        e.preventDefault();
        dispatch(isModalLoginFunc(true))
    }



    return(
        <Fragment>
        {isLoading === true ? <div className="p-7 w-full flex justify-center items-center mb-10"><div className="w-7 h-7 sm:w-9 sm:h-9 border-4 border-l-gogreen rounded-full animate-spin"></div></div> :
            (
                <div className="customcontainer mx-auto shadow-sm shadow-black-rgba  rounded-md mb-10 p-2 box-border">
                <div className="wrap--desctoko place-content-center lg:pl-14 ">
                            <div className="grid-bio flex justify-center items-center p-3">
                                <div className="flex justify-center items-center rounded-full w-14 h-14  mr-5">
                                    <img 
                                        className="w-full rounded-full"
                                        src={oneStore.photo}
                                        alt="logo toko"
                                    />
                                </div>
                                <div>
                                    <p className="text-sm font-semibold">{oneStore.name}</p>
                                    <p className="text-sm lg:text-base lg:font-light font-extralight">{oneStore.city}, {oneStore.province} </p>
                                </div>
                            </div>

                            <TokoDesc 
                                clas = {'grid-desc'}
                                totalProduct = {oneStore.totalProduct}
                                totalFavorite = {oneStore.favoriteStore}
                                totalReview = {oneStore.totalReview}
                                averageReview = {oneStore.averageReview}
                            />
                            <div className="grid-btn flex flex-col justify-evenly items-center">
                                {accessToken === "" ? 
                                    (<p  onClick={showHiddenModal} className="rounded w-auto h-auto bg-gogreen text-xs lg:text-sm text-white py-2 px-3 hover:bg-gogreen cursor-pointer" >Jadikan Favorit</p>)
                                    :
                                    (
                                    <div onClick={favHandler}>
                                        {isFavClicked ? 
                                        (
                                            <p   className="rounded w-auto h-auto border-2 text-xs lg:text-sm cursor-pointer border-gogreen text-gogreen py-2 px-3 hover:bg-gogreen hover:text-white " >Hapus Favorit</p>
                                        ):(
                                            <p   className="rounded w-auto h-auto bg-gogreen text-xs lg:text-sm text-white py-2 px-3 hover:bg-gogreen cursor-pointer" >{isLoading2?"Loading...":"Jadikan Favorit"}</p>
                                        )}
                                    </div>
                                
                                    )
                                }
                                
                            </div>
                </div>
            </div>
            )
        }
            
        </Fragment>
       
        
    )
}