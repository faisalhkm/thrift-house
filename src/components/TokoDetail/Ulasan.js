import axios from 'axios';
import React, { Fragment, useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import UlasanCard from './UlasanCard'
import ModalPhoto from "./ModalPhoto";
import { isModalPotoUlasanFunc } from "../../action";
import { useSelector } from 'react-redux';

function Ulasan() {
    const { modalPhoto } = useSelector((state) => state.isModalPotoUlasan);
    const [ulasan, setUlasan] = useState([])
    // const [page, setPage] = useState(0);
    // const [limit, setLimit] = useState(4)
    const [rate, setRate] = useState (5)
    const [hasPoto, setHasPoto] = useState(false)
    const params = useParams()
    const [isLoading, setIsLoading] = useState(false)
    const [desc, setDesc] = useState([])

    // const fetchData = async () => {
    //         setIsLoading(true)
    //         const url = `https://thrifthouse.herokuapp.com:443/api/v1/stores/${params.id}/reviews?page=0&rate=${rate}&hasPhoto=${hasPoto}`;
    //         try {
    //         const response = await fetch(url);
    //         const data = await response.json();
    //             setUlasan(data.data.review);
    //             setDesc(data.data);
    //             // setMovies(data.Search || data);
        
    //             setIsLoading(false);
    //         } catch (error) {
    //         console.log(error);
    //         }
    //     };
        
        useEffect(() => {
            setIsLoading(true)
            axios.get(`https://thrifthouse.herokuapp.com:443/api/v1/stores/${params.id}/reviews?page=0&rate=${rate}&${hasPoto ? `hasPhoto=${hasPoto}` : ""}`)
            .then(res=>{
                setUlasan(res.data.data.review);
                setDesc(res.data.data);
                setIsLoading(false);
            })
            .catch(error=>{
                    console.log(error);
                    setIsLoading(false);
            }) 
          }, [params.id, hasPoto, rate]);
    
        //   const loadMore = () => {
        //     setLimit((oldPage) => {
        //       return oldPage + 5;
        //     });
        //   };
  return (
    <Fragment>
        <div className="customcontainer  mx-auto p-7 mb-10 relative shadow-sm shadow-black-rgba">
            <div className=' wrap--navulasan place-content-center'>
                <div className='grid-rating mb-5'>
                    <div className='flex'>
                        <div className='mr-2'>
                            <img
                                
                                src='/images/big_star.png'
                                alt=''
                            />
                        </div>
                        <div >
                            <span className='font-semibold text-xl'>{Math.round(desc.totalRate)}</span><span className='font-light text-sm'>/5.0</span>
                        </div>
                    </div>
                </div>

                <div className='grid-descrating  mb-3'>
                    <p className='text-sm font-semibold'> {desc.totalSelling} orang berbelanja di toko ini</p>
                    <p className='text-sm'>{`${desc.totalRated} rating`} <span>•</span>{` ${desc.totalReview} ulasan`}</p>
                </div>
                <form className='grid-dgnfoto  content-center mb-5'>
                <label>
                <input type="checkbox"
                    defaultChecked={hasPoto}
                    onChange={() => setHasPoto(!hasPoto)}
                />
                Dengan foto
                </label>
                </form>
                <div className='grid-type overflow-auto scrollbar-hide'>
                    <ul className='flex justify-between'>
                        {rate === 5 ? 
                            (
                                <li onClick={()=>{setRate(5)}}  className='flex border-2 rounded-2xl p-1 px-2 mr-3 bg-slate-100'>
                            <div className='mr-2 w-5 h-5'>
                            <img
                                
                                src='/images/star.png'
                                alt='star'
                            />
                            </div>
                            <p>5</p>
                        </li>
                            ):
                            (
                                <li onClick={()=>{setRate(5)}}  className='flex border-2 rounded-2xl p-1 px-2 mr-3'>
                            <div className='mr-2 w-5 h-5'>
                            <img
                                
                                src='/images/star.png'
                                alt='star'
                            />
                            </div>
                            <p>5</p>
                        </li>
                            )
                        }
                        {rate === 4 ? 
                            (
                                <li onClick={()=>{setRate(4)}}  className='flex border-2 rounded-2xl p-1 px-2 mr-3 bg-slate-100'>
                            <div className='mr-2 w-5 h-5'>
                            <img
                                
                                src='/images/star.png'
                                alt='star'
                            />
                            </div>
                            <p>4</p>
                        </li>
                            ):
                            (
                                <li onClick={()=>{setRate(4)}}  className='flex border-2 rounded-2xl p-1 px-2 mr-3'>
                            <div className='mr-2 w-5 h-5'>
                            <img
                                
                                src='/images/star.png'
                                alt='star'
                            />
                            </div>
                            <p>4</p>
                        </li>
                            )
                        }
                        {rate === 3 ? 
                            (
                                <li onClick={()=>{setRate(3)}}  className='flex border-2 rounded-2xl p-1 px-2 mr-3 bg-slate-100'>
                            <div className='mr-2 w-5 h-5'>
                            <img
                                
                                src='/images/star.png'
                                alt='star'
                            />
                            </div>
                            <p>3</p>
                        </li>
                            ):
                            (
                                <li onClick={()=>{setRate(3)}}  className='flex border-2 rounded-2xl p-1 px-2 mr-3'>
                            <div className='mr-2 w-5 h-5'>
                            <img
                                
                                src='/images/star.png'
                                alt='star'
                            />
                            </div>
                            <p>3</p>
                        </li>
                            )
                        }
                        {rate === 2 ? 
                            (
                                <li onClick={()=>{setRate(2)}}  className='flex border-2 rounded-2xl p-1 px-2 mr-3 bg-slate-100'>
                            <div className='mr-2 w-5 h-5'>
                            <img
                                
                                src='/images/star.png'
                                alt='star'
                            />
                            </div>
                            <p>2</p>
                        </li>
                            ):
                            (
                                <li onClick={()=>{setRate(2)}}  className='flex border-2 rounded-2xl p-1 px-2 mr-3'>
                            <div className='mr-2 w-5 h-5'>
                            <img
                                
                                src='/images/star.png'
                                alt='star'
                            />
                            </div>
                            <p>2</p>
                        </li>
                            )
                        }
                        {rate === 1 ? 
                            (
                                <li onClick={()=>{setRate(1)}}  className='flex border-2 rounded-2xl p-1 px-2 mr-3 bg-slate-100'>
                            <div className='mr-2 w-5 h-5'>
                            <img
                                
                                src='/images/star.png'
                                alt='star'
                            />
                            </div>
                            <p>1</p>
                        </li>
                            ):
                            (
                                <li onClick={()=>{setRate(1)}}  className='flex border-2 rounded-2xl p-1 px-2 mr-3'>
                            <div className='mr-2 w-5 h-5'>
                            <img
                                
                                src='/images/star.png'
                                alt='star'
                            />
                            </div>
                            <p>1</p>
                        </li>
                            )
                        }
                        
                        
                    </ul>
                </div>
               
            </div>
               
        </div>

        {isLoading ? (
            
                    <div className="customcontainer  mx-auto  flex justify-center p-10 mb-5 text-center col-span-4">
                    <div className="w-7 h-7 sm:w-9 sm:h-9 border-4 border-l-gogreen rounded-full animate-spin"></div>
                    </div>
        ) : (
            ulasan.length === 0 ? <div className='customcontainer   mx-auto p-7 flex justify-center font-semibold mb-5'>Data tidak ada</div>:(

            ulasan.map((el, index)=>(
            <UlasanCard
             username = {el.user.username}
             date = {el.createdAt}
             desc = {el.description}
             photos = {el.photos}
             rating = {el.rating}
             key={index}/>
        )
        )
            )
        )}
        
        
         {/* <div  className="w-full h-28 mt-20 flex flex-col justify-between items-center ">
            {isLoading && <h2 className="">Loading...</h2>}
            <button className="w-auto p-3 rounded-lg text-stone-50 bg-gogreen" onClick={loadMore}>Lihat Lainnya</button>
        </div> */}
        {modalPhoto && <ModalPhoto/>}
    </Fragment>
   
  )
}

export default Ulasan