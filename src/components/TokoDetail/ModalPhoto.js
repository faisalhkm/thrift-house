import React from 'react'
import { useDispatch, useSelector} from "react-redux";
import { isModalPotoUlasanFunc } from '../../action';
import buttonCircleClose from "../../assets/button-circle-close.svg";

function ModalPhoto(props) {
    const dispatch = useDispatch();
    const {url} = useSelector((state) => state.isModalPotoUlasan)
  return (
    <div>
        <div className="w-full h-full bg-black-rgba fixed  z-30 left-0 right-0  top-0 bottom-0">
            <div className=" w-64 md:w-100 h-auto rounded-lg p-2 md:p-5 mx-auto shadow-lg bg-gray-50 mt-36">
            <div className="w-full flex justify-between mb-5">
            <p className='font-semibold'>Detail Foto</p>
            <img  src={buttonCircleClose} alt="" onClick={()=>dispatch(isModalPotoUlasanFunc(false))}/>
            </div>
               <div  className='w-full h-1/2'>
                    <img
                        className='w-full'
                        src={url}
                        alt='ulasan'
                    />
               </div>
            </div>
        </div>
    </div>
  )
}

export default ModalPhoto