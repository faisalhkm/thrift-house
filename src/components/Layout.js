import { Outlet } from "react-router-dom";
import Footer from "./Footer";
import Navbar from "./Navbar";
import { useEffect } from "react";
import jwt_decode from "jwt-decode";
import { logout, favAction, favTokoAction } from "../action";
import { useDispatch, useSelector } from "react-redux";

const Layout = () => {
  const { access_token: token, id: userId } = useSelector(
    (state) => state.login
  );
  const dispatch = useDispatch();

  // check if token is expired
  useEffect(() => {
    if (token) {
      const { exp } = jwt_decode(token);
      if (exp < Date.now() / 1000) {
        dispatch(logout());
      }
    }
  }, [dispatch, token]);

  // get product favorite list
  useEffect(() => {
    if (userId) {
      dispatch(favAction(userId));
      dispatch(favTokoAction(userId))
    };
  }, [userId, dispatch]);

  return (
    <>
      <Navbar />
      <Outlet />
      <Footer />
    </>
  );
};

export default Layout;
