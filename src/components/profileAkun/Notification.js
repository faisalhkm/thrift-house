import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { userDestroyNotif } from '../../action';
import InfoNotif from './InfoNotif';
import PesananNotif from './PesananNotif';
import SemuaNotifikasi from './SemuaNotifikasi';

export default function PesananSaya() {
    const [tabs, setTabs] = useState("Semua");
    const [activeNotifId, setActiveNotifId] = useState([]);
    const [newNotifId, setNewNotifId] = useState([]);
    const dispatch = useDispatch();
    const { arrNotifId } = useSelector((state) => state.notif);
    const [isRefresh, setIsRefresh] = useState(true);

    // Running when Button Refresh Click
     useEffect(() => {
           //Remove Redux Notif 
           setActiveNotifId(arrNotifId.map((dt) => dt.id));
           dispatch(userDestroyNotif());
     }, [isRefresh]);

    //  Get New Notif
     useEffect(() => {
           setNewNotifId(arrNotifId.map((dt) => dt.id));
     }, [arrNotifId]);

    const tabsHandler = (e) => {
      setTabs(e.target.textContent);
    };

    const handleRefresh = () => setIsRefresh(!isRefresh);

  return (
    <>
      <div className="sm:w-full mx-auto my-7 px-6 sm:px-0">
        <div className="px-0">
          <div className="thisistabs flex mb-10 justify-around items-center ">
            <div
              className={`p-[10px] sm:px-1 sm:py-4 text-xs lg:text-base cursor-pointer ${
                tabs === "Semua"
                  ? "text-gogreen border-b-[1px] border-b-gogreen"
                  : "text-[#AEAEBC]"
              } `}
              onClick={tabsHandler}
            >
              Semua
            </div>
            <div
              className={`p-[10px] sm:px-1 sm:py-4 text-xs lg:text-base cursor-pointer ${
                tabs === "Pesanan"
                  ? "text-gogreen border-b-[1px] border-b-gogreen"
                  : "text-[#AEAEBC]"
              }`}
              onClick={tabsHandler}
            >
              Pesanan
            </div>

            <div
              className={`p-[10px] sm:px-1 sm:py-4 text-xs lg:text-base cursor-pointer ${
                tabs === "Info"
                  ? "text-gogreen border-b-[1px] border-b-gogreen"
                  : "text-[#AEAEBC]"
              }`}
              onClick={tabsHandler}
            >
              Info
            </div>
          </div>
        </div>

        {tabs === "Semua" ? <SemuaNotifikasi activeNotifId={activeNotifId} tab={tabs} setIsRefresh={handleRefresh} newNotifId={newNotifId}/> : ""}
        {tabs === "Pesanan" ? <PesananNotif activeNotifId={activeNotifId} tab={tabs}  setIsRefresh={handleRefresh}  newNotifId={newNotifId} /> : ""}
        {tabs === "Info" ? <InfoNotif activeNotifId={activeNotifId} tab={tabs} setIsRefresh={handleRefresh}  newNotifId={newNotifId} /> : ""}
      </div>
    </>
  );
}
