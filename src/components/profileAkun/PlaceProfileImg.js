import React, { useEffect, useState } from 'react';
import buttonCircleClose from "../../assets/button-circle-close.svg";
import axios from 'axios';

export const PlaceProfileImg= ({  onChange, imgId, onCancel, id }) => {
  const [dataImg, setDataImg] = useState("");
  const [defaultlogo , setDefaullogo ]=useState("")


  useEffect(()=>{
    axios.get(`https://thrifthouse.herokuapp.com:443/api/v1/users/${id}`)
    .then(respon =>{
        setDefaullogo(respon.data.data.profileImg)

    })
    .catch(err => {
        console.log(err)
    })
  }
  ,[id])

  const handleUploadClose = (e) => {
    setDataImg("");
  };

  const handleUploadChange = (e) => {
    setDataImg(e.target.files[0]);
    onChange(e.target.files[0]);
  };

  const handleCancel = () => {
    onCancel(null);
  };
  
  return (
    <>
      <div className=" relative text-center">
        {/* Disaat Image Sudah Terisi */}
        {dataImg && (
          <div className='w-40 h-40 sm:w-32 sm:h-32 lg:w-40 lg:h-40 bg-neutral-100 flex justify-center items-center p-2 rounded-md'>
            <img
              src={window.URL.createObjectURL(dataImg)}
              className='w-9/12 rounded-full'
              alt=""
            />
            <div onClick={handleUploadClose} className='absolute -top-[10px] -right-[10px] cursor-pointer '>
            <img src={buttonCircleClose} alt="" onClick={handleCancel} />
            </div>
          </div>
        )}

        {/* Disaat Image Masih Kosong */}
        {!dataImg && (
          <div className='relative'>
            <div className='w-40 h-40 sm:w-32 sm:h-32 lg:w-40 lg:h-40 bg-neutral-100 flex justify-center items-center p-2 rounded-md'>
                <img
                    className='w-9/12 rounded-full'
                    src={defaultlogo}
                    alt='profile'
                />
            </div>
            
         

          <div>
            <div className="flex w-full items-center justify-center bg-grey-lighter mt-4 ">
            <label className="w-40 sm:w-36 flex flex-col items-center px-2 py-2 border-2 border-gogreen  rounded-lg cursor-pointer">
            <span className=" text-sm leading-normal text-gogreen">Pilih Gambar</span>
            <input type='file' className="hidden "  onChange={handleUploadChange}/>
            </label>
        </div>
        </div>
          </div>
        )}
      </div>
    </>
  );
};
