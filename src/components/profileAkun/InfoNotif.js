import axios from 'axios'
import React, { useEffect, useState } from 'react'
import { useSelector } from 'react-redux'
import CardNotifikasi from './CardNotifikasi'

export default function InfoNotif({ tab, activeNotifId, newNotifId, setIsRefresh }) {
  const [dataNotif, setDataNotif] = useState([]);
  const [loading, setLoading] = useState(false);
  const { id } = useSelector((state) => state.login);

  useEffect(() => {
    setLoading(true);
    axios
      .get(
        `https://thrifthouse.herokuapp.com:443/api/v1/users/${id}/notifications?type=${tab}`
      )
      .then((respone) => {
        // Compare API Data with New Notif come
        // show only data not sama with id new notif
        const data = respone.data.data.filter(
          (dt) => !newNotifId.includes(dt.id)
        );
        setDataNotif(data);
        setLoading(false);
        setLoading(false);
      })
      .catch((err) => {
        console.log(err);
        setLoading(false);
      });
  }, [newNotifId, id, tab]);

  return (
    <>
      {loading ? (
        <div className="text-center text-gray-400 h-[470px] ">Loading...</div>
      ) : (
        <div className="overflow-y-auto scrollbar-hide h-[470px] ">
          <div className="mr-[10%] flex justify-end capitalize text-gogreen">
            <p className="cursor-pointer" onClick={() => setIsRefresh()}>
              refresh
            </p>
          </div>
          {dataNotif.map((el,index) => (
            <CardNotifikasi
              key={index}
              active={activeNotifId.includes(el.id)}
              img={el.image}
              title={el.title}
              body={el.body}
              status={el.status}
              type={el.type}
              updatedAt={el.updatedAt}
            />
          ))}
        </div>
      )}
    </>
  );
}

