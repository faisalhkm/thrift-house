import React, { useEffect, useState } from 'react'
import axios from 'axios'
import { useSelector } from 'react-redux/es/exports'
import CardPesananSelesai from './CardPesananSelesai'
import CardPesanan from './CardPesanan'

export default function Selesai() {
  const [dataOrder, setDataOrder] = useState([])
  const [loading, setLoading] = useState(false)
  const {id} = useSelector((state) => state.login);

  useEffect(()=>{
    setLoading(true)
    axios.get(`https://thrifthouse.herokuapp.com:443/api/v1/users/${id}/orders?status=selesai`)
    .then(res => {
      setDataOrder(res.data.data)
      setLoading(false)
    })
    .catch(err =>{
      console.log(err)
      setLoading(false)
    })
  },[id])
  
  return (
    <>
    {loading ? <div className='text-center text-gray-400 h-[470px] '>Loading...</div> : (
      <div className='overflow-auto mb-5'>
        <div className='h-[470px] overflow-y-auto scrollbar-hide p-5'>
          {dataOrder.length === 0 ? <div className='text-center text-gray-400'>Belum ada pesanan</div> : 
            dataOrder.map((el, index) => 
            (
              <CardPesananSelesai
                  key = {index}
                  id = {el.id}
                  orderCode = {el.orderCode}
                  status = {el.packagingStatus}
                  store = {el.store}
                />
            )
          )   
        }
        
        </div>
    </div>
    )}

    </>
  )
}
