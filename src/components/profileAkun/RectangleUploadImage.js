import React, { useState } from 'react'
// import plusCircle from "../../assets/icon/plus-circle.svg";
import buttonCircleClose from "../../assets/button-circle-close.svg";


export const RectangleUploadImage = ({ title, onChange, imgId, onCancel }) => {
  const [dataImg, setDataImg] = useState("");

  const handleUploadClose = (e) => {
    setDataImg("");
  };

  const handleUploadChange = (e) => {
    setDataImg(e.target.files[0]);
    onChange(imgId, e.target.files[0]);
  };

  const handleCancel = () => {
    onCancel(imgId);
  };
  
  return (
    <>
      <div className="relative text-center w-1/5 lg:w-1/5">
        {/* Disaat Image Sudah Terisi */}
        {dataImg && (
          <div className='h-20 lg:h-36  w-full'>
            <img
              src={window.URL.createObjectURL(dataImg)}
              className="h-full  w-full rounded-md"
              alt=""
            />
            <div
              className="absolute -top-[10px] -right-[10px] cursor-pointer"
              onClick={handleUploadClose}
            >
             <img src={buttonCircleClose} alt="" onClick={handleCancel} />
            </div>
          </div>
        )}

        {/* Disaat Image Masih Kosong */}
        {!dataImg && (
          <div>
            <label
              htmlFor={`actual-btn${imgId}`}
              className="h-20 w-full border-2 rounded-md border-dashed flex justify-center items-center"
            >
              +
            </label>

            <input
              type="file"
              id={`actual-btn${imgId}`}
              hidden
              onChange={handleUploadChange}
            />
          </div>
        )}
        {/* <p className="text-sm mt-2">{title}</p> */}
      </div>
    </>
  );
};
