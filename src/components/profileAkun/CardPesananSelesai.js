import React from 'react'
import { useDispatch} from "react-redux";
import { Link } from 'react-router-dom';
import { displayModalTransaksi, displayModalUlasan } from '../../action';

export default function CardPesananSelesai({  id ,  status,  store }) {
  const dispatch = useDispatch();
  // const [store, setstore] = useState([])
  // const [alamat, setalamat] = useState([])
  // const [orderDetail, setOrderDetail] = useState([])
  // const [loading, setLoading] = useState(false)
  // const {id} = useSelector((state) => state.login);
  // const {idOrder} = useSelector((state) => state.isModalTransaksi);
  // const i =0
  


  // useEffect(()=>{
  //   setLoading(true)
  //   axios.get(`https://thrifthouse.herokuapp.com:443/api/v1/users/${id}/orders/${idOrder}`)
  //   .then(res => {
  //     setOrderDetail(res.data.data)
  //     setstore(res.data.data.store)
  //     setalamat(res.data.data.address)
  //     setLoading(false)
  //   })
  //   .catch(err =>{
  //     console.log(err)
  //     setLoading(false)
  //   })
  // },[id, idOrder])
  

  
  return (
    <>

        <div className="w-full p-10 border-2 mb-10">
        <div className='flex justify-between items-center '>
        <div className='w-full flex flex-col justify-between items-start '>
        
            {store.map(el => 
              (
              <div className=' w-full border-2 rounded-lg p-4 '>
              <div className='flex'>
                <div className='w-full flex '>
                <div className="flex justify-center items-center rounded-full w-14 h-14  mr-3">
                          <img 
                          className="w-full "
                          src={el.photo}
                          alt="logo toko"
                          />
                      </div>
                      <div>
                      <p className="text-sm font-bold lg:text-base">{el.name}</p>
                      <p className="text-sm   font-extralight">{el.city}, {el.province} </p>
                      </div>
                     
                </div>
                <div className='w-1/4 flex justify-end p-3'>
                      <p  className='text-xs w-32 h-9 sm:h-13 flex justify-center items-center lg:h-7 lg:w-40 mb-5 bg-[#49BC19] py-1 px-1 rounded-md text-center text-white'>{status}</p>
                      </div>
              </div>
             
              
                <div className='flex justify-between'>
                    <p className="font-bold text-xs md:text-sm mb-2 text-black">Detail Produk</p>
                    <p className="font-bold text-[10px] md:text-xs mb-2 text-black">{el.name} <span className='ml-1 text-sm'>{">"}</span></p>
                </div>

                {el.products.map(el1 => (
                  <div className='w-full flex flex-wrap justify-between items-center p-2 mt-5 '>
                <div className='flex items-center '>
                <div className="flex justify-center items-center rounded-full w-10 h-10  mr-2 ">
                        <img 
                        className="w-full "
                        src={el1.photo}
                        alt="produk"
                        />
                    </div>
                    <div className='flex flex-col justify-center'>
                        <p className='font-bold text-xs lg:text-base'>{el1.brand}</p>
                        <p className='text-xs lg:text-sm '>{el1.name}</p>
                    </div>
                </div>
               
                <div className='flex flex-col'>
                    <p className='text-xs bg-[#F2F2F2] text-[#6A6A6A] flex justify-center w-auto mb-3'>{el1.size}</p>
                    <p className='text-xs bg-[#F2F2F2] text-[#6A6A6A] flex justify-center w-auto'>{el1.condition}</p>
                </div>
                <div className=' mt-5 lg:mt-0 w-1/2 lg:w-auto '>
                <p className='text-xs lg:text-base'>{1} item <span>({el1.weight} gram)</span></p>
                <p className='text-xs font-semibold lg:text-base'>Rp{el1.price.toLocaleString("id-ID")}</p>
                </div>
            </div>

                ))}

                <div className='flex justify-end p-4'>
                    <button
                    onClick={e => dispatch(displayModalUlasan({modal:true, id:id, idToko:el.id}))}
                    className="rounded  border-2  w-[183px] lg:w-auto text-xs   bg-gogreen text-white py-3 px-6 mr-5 flex items-center justify-center hover:bg-green-600"
                  >
                    Beri Ulasan
                  </button>    
                </div>

                            
              </div>
              
            )            
            )}                    
            </div>
          
        </div>
        <div className=" flex flex-wrap justify-end mt-2">
              <button
              onClick={e => dispatch(displayModalTransaksi({modal:true, id:id}))}
                className="rounded  border-2  w-[183px] lg:w-auto text-xs  border-gogreen text-gogreen py-3 px-6 mr-5 flex items-center justify-center  hover:bg-gogreen hover:text-white mb-2 lg:mb-0 "
              >
                Detail Transaksi
              </button>
              {status === "Menunggu pembayaran" ? (
                <Link to={`/keranjang/payment/${id}`}
                className="rounded text-xs  bg-gogreen text-white py-3 px-6 mr-5 flex items-center justify-center hover:bg-green-600"
              >
                Lanjut ke pembayaran
              </Link>
              ):""}

             
            </div>
            </div>
       
    </>
  )
}
