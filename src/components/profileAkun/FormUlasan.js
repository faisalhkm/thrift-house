import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector} from "react-redux";
import { displayModalUlasan } from '../../action';
import axios from 'axios';
import { RectangleUploadImage } from './RectangleUploadImage';
import { useNavigate } from 'react-router-dom';

export default function FormUlasan() {
  const dispatch = useDispatch();
  const [orderDetail, setOrderDetail] = useState([])
  const [store, setstore] = useState([])
  const {id} = useSelector((state) => state.login);
  const {idOrderUlasan} = useSelector((state) => state.isModalUlasan);
  const {idStore} = useSelector((state) => state.isModalUlasan);
  const [berhasil, setBerhasil] = useState (false);
  const [modal, setModal] = useState(false);
  const [gagal, setGagal] = useState (false);
  const [isloading, setIsloading] = useState(false)
  const [error, setError] = useState([]);
  const [loding, setloding] = useState(false)
  
  //input form
  const photosPattern = [
    {
      id: 0,
      value: "",
    },
    {
      id: 1,
      value: "",
    },
    {
      id: 2,
      value: "",
    },
    {
      id: 3,
      value: "",
    },
  ];
  const [photos, setPhotos] = useState(photosPattern)
  const [rating, setRating] = useState(0)
  const [anonim, setAnonim] = useState(false)
  const navigate=useNavigate()
  const [form, setForm] = useState({
    description: ""
  });
  
  const handleAddPhoto = (imgId, dataImg) => {
    const arrPhotos = photos.map((photo) => {
      if (photo.id === imgId) {
       return {
          id: imgId,
          value: dataImg,
        };
      }
     return photo;
    });
    setPhotos(arrPhotos);
  }

  const handleCancelPhoto = (imgId) => {
    const arrPhotos = photos.map((photo) => {
      if (photo.id === imgId) {
       return {
          id: imgId,
          value: "",
        };
      }
     return photo;
    });
    setPhotos(arrPhotos);
  }

  // const handleImageChange = async(e) => {
  //   if (e.target.files) {
  //     const filesArray = Array.from(e.target.files).map((file) =>
  //       URL.createObjectURL(file)
  //     );

  //     setdisplayimg((prevImages) => prevImages.concat(filesArray));
  //     Array.from(e.target.files).map(
  //       (file) => URL.revokeObjectURL(file) 
  //     );
  //   }
  //   if (e.target.files) {
  //     const filesArray = Array.from(e.target.files).map((file) => file);
  //     setdataimg((prevImages) => prevImages.concat(filesArray));
  //     Array.from(e.target.files).map(
  //       (file) => file
  //     );
  //   }
  // };

  const formHandler = (e) => {
    setForm((prev) => ({ ...prev, [e.target.name]: e.target.value }));
  };
  
  // const renderPhotos = (source) => {
  //   // console.log("source: ", source);
  //   return source.map((photo) => {
  //     return <img src={photo} alt="" key={photo} className='w-[150px] h-[150px] object-cover p-1' />;
  //   });
  // };


  useEffect(()=>{
    
    axios.get(`https://thrifthouse.herokuapp.com:443/api/v1/users/${id}/orders/${idOrderUlasan}`)
    .then(res => {
      res.data.data.store.forEach(element => {
        if(element.id === idStore){
          setOrderDetail(element)
          setstore(element.products)
        }
      });

      
    })
    .catch(err =>{
      console.log(err)
    })
  },[id, idOrderUlasan,idStore])


  const handleSubmit = async (e) => {
    setloding(true)
    e.preventDefault();
   const formData = new FormData();
   formData.append("anonim", anonim);
   formData.append("description", form.description );
   photos.forEach(el=>{
    if(el.value===""){
      formData.append("photos", JSON.stringify([]))
     }else{
       photos.forEach((photo) => {
        formData.append("photos", photo.value);
      });
     }
   })
  
   formData.append("rating", rating);
   formData.append("userId", id );
    
   try {
     const test = await axios.postForm(
       `https://thrifthouse.herokuapp.com:443/api/v1/stores/${idStore}/reviews`,
       formData,
        {
          headers: {
            "Content-Type": "multipart/form-data",
          },
        }
       
     );
     console.log(test);
     setModal(false);
     setBerhasil(true)
     setIsloading(false)
     setloding(false)

   } catch (error) {
    console.log(error)
    setError(error)
    setModal(false)
    setGagal(true)
    setIsloading(false)
    setloding(false)
   }
}


//   function handleModal(e) {
//     e.preventDefault(); 
//     setModal(true)
// }

function sureModal(e) {
    return (
        <>
            {isloading ? (
                <div className="w-full bg-black-rgba fixed z-50 top-0 bottom-0 left-0 right-0 p-6 flex justify-center items-center mx-auto "><div className='bg-white p-10'><div className="w-7 h-7 sm:w-9 sm:h-9 border-4 border-l-gogreen rounded-full animate-spin"></div></div></div>
            )
            :
            (
                <div className="bg-black-rgba fixed flex z-50 top-0 bottom-0 left-0 right-0 p-6">
            <div className="bg-white rounded-lg m-auto  overflow-hidden pb">
            <div className=" px-14 pt-7 pb-6">
                <p className="font-bold text-sm mb-2">Apakah kamu yakin?</p>
                <p className="text-xs">
                Jika tekan <span className='text-gogreen font-semibold'>Konfirmasi</span> hanya jika biodata diri sudah sesuai.
                </p>
            </div>
            <div className="flex justify-end p-2">
                <button
                type="button"
                onClick={(e) => setModal(!modal)}
                className="py-2 px-7 text-xs   hover:bg-gray-100 hover:text-gray-600 text-white rounded-md bg-gogreen font-semibold mr-5"
                >
                Batalkan
                </button>
                <button
                onClick={handleSubmit}
                className="py-2 px-7 text-xs font-medium border-2 border-gogreen rounded-md text-gogreen hover:bg-gray-50 "
                >
                Konfirmasi
                </button>
            </div>
            </div>
        </div>
            )}
           
        </>
        
    );
  }

  function Berhasil(e) {
    return (<>
                 <div className="bg-black-rgba fixed flex z-50 top-0 bottom-0 left-0 right-0 p-6">
                <div className="bg-white rounded-lg m-auto  overflow-hidden">
                <div className=" px-14 pt-7 pb-6">
                    <p className="font-bold text-sm mb-2">Ulasan Terkirim</p>
                    <p className="text-xs">
                    Terimakasih, ulasan kamu sangat berarti buat kami
                    </p>
                </div>
                <div className="flex">
                    <button
                    type="button"
                    onClick={(e) => {dispatch(displayModalUlasan({modal:false, id:"", idToko:""}));setBerhasil(!berhasil)}}
                    className="flex-grow py-4 text-xsborder-[0.3px] hover:bg-gray-100 text-gogreen font-semibold"
                    >
                    Ok
                    </button>
                </div>
                </div>
            </div>

    </>
    );
  }

  function Gagal(e) {
    return (
      <><div className="bg-black-rgba fixed flex z-50 top-0 bottom-0 left-0 right-0 p-6">
            <div className="bg-white rounded-lg m-auto  overflow-hidden">
            <div className=" px-14 pt-7 pb-6">
                <p className="font-bold text-sm mb-2">Gagal Mengirimkan Ulasan</p>
                <p className="text-xs">
                Pastikan inputan yang kamu masukan sudah benar
                </p>
            </div>
            <div className="flex">
                <button
                type="button"
                onClick={(e) => setGagal(!gagal)}
                className="flex-grow py-4 text-xsborder-[0.3px] hover:bg-gray-100 text-gogreen font-semibold"
                >
                Ok
                </button>
            </div>
            </div>
        </div>
    </>
        
    );
  }





  return (
    <>

        <div className="w-full">
        <div className='flex justify-between items-center'>
        <div className='w-full flex flex-col justify-between items-start '>

              <div className=' w-full mb-5'>
              <div className='flex justify-between'>
                        <p className="font-bold text-xs md:text-sm mb-2 text-black">Detail Produk</p>
                        <p className="font-bold text-[10px] md:text-xs mb-2 text-black hover:text-slate-600 cursor-pointer"
                        onClick={()=>{
                    navigate(`/toko/${orderDetail.id}`); 
                    dispatch(displayModalUlasan({modal:false, id:""}))
                    }}>{orderDetail.name} <span className='ml-1 text-sm'>{">"}</span></p>
                      </div>

                {store.map(el1 => (
                  <div className='w-full flex flex-wrap justify-between items-center p-2 mt-5' >
                <div className='flex items-center '>
                <div className="flex justify-center items-center rounded-full w-10 h-10  mr-2 ">
                        <img 
                        className="w-full "
                        src={el1.photo}
                        alt="produk"
                        />
                    </div>
                    <div className='flex flex-col justify-center'>
                        <p className='font-bold text-xs lg:text-base'>{el1.brand}</p>
                        <p className='text-xs lg:text-sm '>{el1.name}</p>
                    </div>
                </div>
               
                <div className='flex flex-col'>
                    <p className='text-xs bg-[#F2F2F2] text-[#6A6A6A] flex justify-center w-auto mb-3'>{el1.size}</p>
                    <p className='text-xs bg-[#F2F2F2] text-[#6A6A6A] flex justify-center w-auto'>{el1.condition}</p>
                </div>
                <div className=' mt-5 lg:mt-0 w-1/2 lg:w-auto '>
                <p className='text-xs lg:text-base'>{1} item <span>({el1.weight} gram)</span></p>
                <p className='text-xs font-semibold lg:text-base'>Rp{el1.price.toLocaleString("id-ID")}</p>
                </div>
            </div>

                ))}
                    <form className='flex flex-col items-center justify-center mt-10' onSubmit={handleSubmit}>
                        <p className='font-bold text-xs md:text-sm mb-2 text-black'>Beri Ulasan</p>
                        <p className='text-xs md:text-sm mb-2 text-gray-500'>Berikan ulasan untuk produk ini</p>
                        <div className="star-rating">
                            {[...Array(5)].map((star, index) => {
                              index += 1;
                              return (
                                <button
                                  type="button"
                                  key={index}
                                  className={`${index <= rating ? "text-[#D8A901]" : "text-[#999]"} bg-transparent border-0 outline-none cursor-pointer text-5xl sm:text-7xl`}
                                  onClick={() => setRating(index)}
                                >
                                  <span className="star">&#9733;</span>
                                </button>
                              );
                            })}
                      </div>
                        <p className='font-bold text-xs md:text-sm mb-2 text-black mt-10'>Tuliskan Ulasanmu</p>
                        <p className='text-xs md:text-sm mb-2 text-gray-500'>Berikan ulasan untuk produk ini</p>
                        <textarea id="message" name='description' rows="4" class="block p-2.5 w-full text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300" placeholder="Tulis ulasan prduk disini... " onChange={formHandler}></textarea>
                        

                        {/* <div className='w-full '>
                        <div className="result  w-full mt-4 flex flex-wrap items-center justify-start">{renderPhotos(displayimg)}</div>
                        <div>
                          <input type="file" id="file" multiple onChange={handleImageChange} className='hidden'/>
                          <div className="label-holder w-full h-[50px] mt-0 flex items-center">
                            <label htmlFor="file" className="label  bg-gogreen text-white  text-sm cursor-pointer flex p-3 rounded-lg">
                            <Icon icon="material-symbols:add-circle-outline" /> 
                            <p className='text-xs ml-2'>Tambah Foto dan Video</p>
                            </label>
                          </div>
                         
                        </div>
                        </div> */}
                        <div className="w-full flex justify-evenly mt-5">
                          <RectangleUploadImage
                            title="Foto Review 1"
                            imgId={0}
                            key={0}
                            onChange={(imgId, dataImg) => handleAddPhoto(imgId, dataImg)}
                            onCancel={(imgId) => handleCancelPhoto(imgId)}
                          />
                          <RectangleUploadImage
                            title="Foto Review 2"
                            imgId={1}
                            key={1}
                            onChange={(imgId, dataImg) => handleAddPhoto(imgId, dataImg)}
                            onCancel={(imgId) => handleCancelPhoto(imgId)}
                          />
                          <RectangleUploadImage
                            title="Foto Review 3"
                            imgId={2}
                            key={2}
                            onChange={(imgId, dataImg) => handleAddPhoto(imgId, dataImg)}
                            onCancel={(imgId) => handleCancelPhoto(imgId)}
                          />
                          <RectangleUploadImage
                            title="Foto Review 4"
                            imgId={3}
                            key={3}
                            onChange={(imgId, dataImg) => handleAddPhoto(imgId, dataImg)}
                            onCancel={(imgId) => handleCancelPhoto(imgId)}
                          />
                        </div>

                        <div className='w-full mt-4'> 
                        <label className='text-xs md:text-sm mb-2 text-gray-500 '>
                            <input type="checkbox"
                                onChange={(e) =>
                                    setAnonim( e.target.checked)
                                }
                            />
                        Tampilkan username pada penilaian
                        </label>
                        </div>
                        
                        <div className=' w-full flex justify-end'>
                          <button
                          onClick={e => dispatch(displayModalUlasan(false))}
                          className="py-2 px-7 text-xs font-medium border-2 border-gogreen rounded-md text-gogreen hover:bg-gray-50 mr-5 "
                          >
                          Batalkan
                          </button>
                          <button
                          className="py-2 px-7 text-xs   hover:bg-gray-100 hover:text-gray-600 text-white rounded-md bg-gogreen font-semibold mr-5"
                          >
                          {loding ? "Loading..." : "Kirim Ulasan"}
                          </button>
                        </div>
                        
                        
                    </form>
                
              </div>
              

            </div>
        </div>
            </div>
            {isloading && sureModal()}
            {berhasil && Berhasil()}
            {gagal && Gagal()}
    
       
    </>
  )
}
