import { useRef, useState, useEffect } from "react";
import { Icon } from "@iconify/react";
import axios from "axios";
import Spinner from "../Spinner";

const ModalCourier = ({
  isCourierOpen,
  setIsCourierOpen,
  setShippingCost,
  setSelectedService,
  item,
  selectedAlamat,
  selectedCourier,
}) => {
  const closeModalOutside = useRef();

  const [tempSelectedService, setTempSelectedService] = useState("");
  const [tempShippingCost, setTempShippingCost] = useState({});
  const [service, setService] = useState([]);

  // get list service
  useEffect(() => {
    // .post(`https://thrifthouse.herokuapp.com/api/v1/rajaongkir/cost`, {
    //   courier: tempSelectedCourier,
    //   destination: item.store.city_id,
    //   origin: selectedAlamat.idCity,
    //   weight: item.product[0].weight,
    // })

    axios
      .get(
        `https://rajaongkir.vercel.app/cost?origin=${selectedAlamat.idCity}&destination=${item.store.city_id}&weight=${item.product[0].weight}&courier=${selectedCourier}`
      )
      .then((res) => {
        setService(res.data.rajaongkir.results[0].costs);
      })
      .catch((err) => console.log(err));
  }, [
    item.product,
    item.store.city_id,
    selectedAlamat.idCity,
    selectedCourier,
  ]);

  // set selected service to state
  const selectedServiceHandler = (item) => {
    setTempSelectedService(item.service);
    setTempShippingCost(item.cost[0]);
  };

  // handle submit when choose service on modal
  const submitSelectedServiceHandler = () => {
    if (tempSelectedService) {
      setIsCourierOpen(false);
      setSelectedService(tempSelectedService);
      setShippingCost(tempShippingCost);
    }
  };

  return (
    <div
      className={`modal ${
        isCourierOpen ? "block" : "hidden"
      } justify-center items-center fixed z-10 left-0 top-0 w-full h-full overflow-auto bg-[#1F1F1F]/60 py-[145px]`}
      ref={closeModalOutside}
      onClick={(e) =>
        e.target === closeModalOutside.current && setIsCourierOpen(false)
      }
    >
      <div className="modalContent bg-white max-w-[947px] mx-6 md:mx-auto px-6 md:px-20 py-10 rounded-lg relative">
        <Icon
          icon="ph:x-circle-fill"
          height="40"
          className="absolute right-5 md:right-10 top-7 cursor-pointer"
          onClick={() => setIsCourierOpen(false)}
        />

        <h3 className="font-bold text-2xl text-center mb-3">
          Pilih Services Pengiriman
        </h3>
        <p className="text-[#8F8F8F] text-center text-lg mb-10 max-w-[578px] mx-auto">
          Estimasi tanggal diterima tergantung pada waktu pengemasan Penjual dan
          waktu pengiriman ke lokasi kamu
        </p>

        <div className="thisisservicecontainer mb-16">
          {service.length !== 0 ? (
            service.map((item) => (
              <div
                key={item.service}
                className={`thisisservicepengiriman mb-10 border-[1px] p-6 rounded-lg flex items-center justify-between space-x-6 cursor-pointer hover:border-gogreen ${
                  tempSelectedService === item.service
                    ? "border-gogreen"
                    : "border-[#CECFD7]"
                }`}
                onClick={() => selectedServiceHandler(item)}
              >
                <div className="font-bold text-xl">
                  {item.description} ({item.service})
                </div>
                <div className="text-lg text-right whitespace-nowrap">
                  Akan diterima pada {item.cost[0].etd} hari
                </div>
                <div className="font-bold text-xl text-gogreen">
                  Rp{item.cost[0].value}
                </div>
              </div>
            ))
          ) : (
            <Spinner className={"mx-auto"} />
          )}
        </div>

        <div
          className="font-medium text-lg text-center text-white bg-gogreen hover:bg-gogreen-hover py-[14px] rounded-lg cursor-pointer"
          onClick={submitSelectedServiceHandler}
        >
          Pilih
        </div>
      </div>
    </div>
  );
};

export default ModalCourier;
