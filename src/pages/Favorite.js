import { InlineIcon } from "@iconify/react";
import { useState } from "react";
import ProductFav from "../components/favorite/ProductFav";
import StoreFav from "../components/favorite/StoreFav";
import axios from "axios";
import { useSelector } from "react-redux";

const Favorite = () => {
  const [productFav, setProductFav] = useState([]);
  const [storeFav, setStoreFav] = useState([]);
  const [tabs, setTabs] = useState("Produk Favorit");
  const [isFilterOpen, setIsFilterOpen] = useState(false);
  const [query, setQuery] = useState("");
  const [filter, setFilter] = useState({
    text: "Terbaru Disimpan",
    param: "terbaru_disimpan",
  });
  const { id } = useSelector((state) => state.login);

  // handle tab change
  const tabsHandler = (e) => {
    setTabs(e.target.textContent);
  };

  // handle filter open
  const filterHandler = () => setIsFilterOpen((prev) => !prev);

  // handle search
  const searchHandler = (e) => {
    e.preventDefault();
    if (tabs === "Produk Favorit") {
      axios
        .get(
          `https://thrifthouse.herokuapp.com/api/v1/users/${id}/products/favorites?size=8&page=0&search=${query}&urutkan=${filter.param}`
        )
        .then((res) => setProductFav(res.data.data))
        .catch((err) => console.log(err));
    }

    if (tabs === "Toko Favorit") {
      axios
        .get(
          `https://thrifthouse.herokuapp.com/api/v1/users/${id}/stores/favorites?size=8&page=0&search=${query}&urutkan=${filter.param}`
        )
        .then((res) => setStoreFav(res.data.data))
        .catch((err) => console.log(err));
    }
  };

  // handle filter
  const filterHandlerFetch = (filterText, filterParam) => {
    if (tabs === "Produk Favorit") {
      axios
        .get(
          `https://thrifthouse.herokuapp.com/api/v1/users/${id}/products/favorites?size=8&page=0&urutkan=${filterParam}`
        )
        .then((res) => {
          setProductFav(res.data.data);
          setIsFilterOpen(false);
          setFilter({ text: filterText, param: filterParam });
        })
        .catch((err) => console.log(err));
    }

    if (tabs === "Toko Favorit") {
      axios
        .get(
          `https://thrifthouse.herokuapp.com/api/v1/users/${id}/stores/favorites?size=8&page=0&urutkan=${filterParam}`
        )
        .then((res) => {
          setStoreFav(res.data.data);
          setIsFilterOpen(false);
          setFilter({ text: filterText, param: filterParam });
        })
        .catch((err) => console.log(err));
    }
  };

  return (
    <div className="customcontainer mx-auto my-7 px-6 sm:px-0">
      <div className="thisistabs flex mb-10">
        <div
          className={`p-[10px] sm:px-7 sm:py-4 text-xs sm:text-xl cursor-pointer ${
            tabs === "Produk Favorit"
              ? "text-gogreen border-b-[1px] border-b-gogreen"
              : "text-[#AEAEBC]"
          } `}
          onClick={tabsHandler}
        >
          Produk Favorit
        </div>
        <div
          className={`p-[10px] sm:px-7 sm:py-4 text-xs sm:text-xl cursor-pointer ${
            tabs === "Toko Favorit"
              ? "text-gogreen border-b-[1px] border-b-gogreen"
              : "text-[#AEAEBC]"
          }`}
          onClick={tabsHandler}
        >
          Toko Favorit
        </div>
      </div>

      <div className="thisisfiltercontainer flex items-center justify-between mb-10">
        <div className="thisissearch border-[1px] border-[#CFCFCF] py-[10px] sm:py-[14px] px-[11px] sm:px-[21px] rounded-md flex items-center space-x-2 mr-6 w-2/3 sm:w-1/3">
          <span className="inline">
            <InlineIcon
              icon="akar-icons:search"
              color="#CFCFCF"
              height="24"
              className="w-5 h-5 sm:w-6 sm:h-6"
            />
          </span>
          <form className="w-full" onSubmit={searchHandler}>
            <input
              type="text"
              placeholder="Cari produk favoritmu"
              className=" placeholder:text-[#CFCFCF] placeholder:text-sm sm:placeholder:text-base focus:outline-none w-full"
              onChange={(e) => setQuery(e.target.value)}
            />
          </form>
        </div>

        <div className="thisisfilter flex items-center">
          <div className="mr-8 font-medium text-[#B4B4B4] hidden sm:block">
            Urutkan
          </div>
          <div className="relative">
            <InlineIcon
              icon="ph:arrows-down-up-fill"
              height="24"
              className="sm:hidden"
              onClick={filterHandler}
            />
            <div
              className={`px-4 py-[14px] border-[1px] border-gogreen rounded-lg cursor-pointer text-gogreen arrowgreen bg-white z-30 relative hidden sm:flex items-center ${
                isFilterOpen === true ? "arrowreverse" : ""
              }`}
              onClick={filterHandler}
            >
              {filter.text}
            </div>
            <ul
              className={`${
                isFilterOpen === true ? "absolute" : "hidden"
              } sm:w-full text-center bg-white border-[1px] border-gogreen rounded-lg sm:pt-5 top-8 right-0 z-20`}
            >
              <li
                className="py-[14px] cursor-pointer hover:text-gogreen text-xs sm:text-base px-5 whitespace-nowrap"
                onClick={() =>
                  filterHandlerFetch("Terbaru Disimpan", "terbaru_disimpan")
                }
              >
                Terbaru Disimpan
              </li>
              <li
                className="py-[14px] cursor-pointer hover:text-gogreen text-xs sm:text-base px-5 whitespace-nowrap"
                onClick={() =>
                  filterHandlerFetch("Terakhir Disimpan", "terakhir_disimpan")
                }
              >
                Terakhir Disimpan
              </li>
              <li
                className="py-[14px] cursor-pointer hover:text-gogreen text-xs sm:text-base px-5 whitespace-nowrap"
                onClick={() =>
                  filterHandlerFetch("Abjad Awal (A-Z)", "abjad_awal")
                }
              >
                Abjad Awal (A-Z)
              </li>
              <li
                className="py-[14px] cursor-pointer hover:text-gogreen text-xs sm:text-base px-5 whitespace-nowrap"
                onClick={() =>
                  filterHandlerFetch("Abjad Akhir (Z-A)", "abjad_akhir")
                }
              >
                Abjad Akhir (Z-A)
              </li>
            </ul>
          </div>
        </div>
      </div>

      {/* render fav content */}
      {tabs === "Produk Favorit" ? (
        <ProductFav
          productFav={productFav}
          setProductFav={setProductFav}
          query={query}
          filter={filter}
        />
      ) : (
        <StoreFav
          storeFav={storeFav}
          setStoreFav={setStoreFav}
          query={query}
          filter={filter}
        />
      )}
    </div>
  );
};

export default Favorite;
