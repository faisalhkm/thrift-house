import axios from "axios";
import { useState } from "react";
import { useNavigate, Link } from "react-router-dom";
import { InlineIcon } from "@iconify/react";
import Spinner from "../components/Spinner";

export default function Register() {
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [phone, setPhone] = useState("");
  const [password, setPassword] = useState("");
  const [isShowPassword, setIsShowPassword] = useState(false);
  const [errMsg, setErrMsg] = useState("");
  const [isLoading, setIsLoading] = useState(false);

  const navigate = useNavigate();

  const handleSubmit = (e) => {
    e.preventDefault();
    setIsLoading(true);
    axios
      .post("https://thrifthouse.herokuapp.com/register", {
        email: email,
        password: password,
        phone: phone,
        role: "ROLE_USER",
        username: username,
      })
      .then(() => {
        navigate("/login");
        setErrMsg("");
        setIsLoading(false);
      })
      .catch((error) => {
        setIsLoading(false);
        const errMsg = "telah digunakan";
        if (error.response.data.message === "Username already exists!") {
          setErrMsg(`Username ${errMsg}`);
        }
        if (error.response.data.message === "Email already exists!") {
          setErrMsg(`Email ${errMsg}`);
        }
        setTimeout(() => {
          setErrMsg("");
        }, 3000);
      });
  };

  return (
    <div className="container my-9 px-5 sm:max-w-lg sm:mx-auto xl:flex xl:max-w-6xl xl:gap-4">
      <img src="/images/register.png" alt="login" className="mx-auto w-40 sm:w-[420px] lg:w-[574px] object-contain" />
      <div>
        <div className="text-center mt-6">
          <h1 className="font-bold text-[#29A867] sm:text-[40px]">Daftarkan akunmu</h1>
          <p className="text-slate-400 text-sm mt-1 mb-6 sm:text-lg">Masukkan data untuk melanjutkan</p>
        </div>
        <form onSubmit={handleSubmit} className="">
          <label className="" htmlFor="username">
            <span className="text-sm leading-5 mb-1 block">Username</span>
          </label>
          <input
            id="username"
            className="h-9 border-[#CFCFCF] border rounded-lg w-full text-xs py-3 px-2 mb-4 placeholder:text-slate-300 focus:outline-none focus:ring-1 block focus:ring-[#4DB680]"
            type="text"
            placeholder="Masukkan nama kamu"
            value={username}
            onChange={(e) => setUsername(e.target.value)}
            required
          />
          <label className="" htmlFor="email">
            <span className="text-sm leading-5 mb-1 block">Email</span>
          </label>
          <input
            id="email"
            className="h-9 border-[#CFCFCF] border rounded-lg w-full text-xs py-3 px-2 mb-4 placeholder:text-slate-300 focus:outline-none focus:ring-1 block focus:ring-[#4DB680]"
            type="email"
            placeholder="Masukkan email kamu"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            required
          />
          <label className="" htmlFor="phone">
            <span className="text-sm leading-5 mb-1 block">Nomor HP</span>
          </label>
          <input
            id="phone"
            className="h-9 border-[#CFCFCF] border rounded-lg w-full text-xs py-3 px-2 mb-4 placeholder:text-slate-300
        focus:outline-none focus:ring-1 block focus:ring-[#4DB680]"
            type="text"
            placeholder="Masukkan nomor HP kamu"
            value={phone}
            onChange={(e) => setPhone(e.target.value)}
            pattern="^(^\+62\s?|^0)(\d{3,4}-?){2}\d{3,4}$"
            required
          />
          <label className="" htmlFor="password">
            <span className="text-sm leading-5 mb-1 block">Kata Sandi</span>
          </label>
          <div className="relative">
            <input
              id="password"
              className="h-9 border-[#CFCFCF] border rounded-lg w-full text-xs py-3 px-2 placeholder:text-slate-300
        focus:outline-none focus:ring-1 block focus:ring-[#4DB680]"
              type={isShowPassword ? "text" : "password"}
              placeholder="Masukkan kata sandi kamu"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
              required
            />
            {isShowPassword ? (
              <InlineIcon icon="ri:eye-line" onClick={() => setIsShowPassword(!isShowPassword)} className="absolute cursor-pointer top-1/2 -translate-y-1/2 right-2 text-[#b6b6b6] lg:w-5 lg:h-5" />
            ) : (
              <InlineIcon icon="ri:eye-off-line" onClick={() => setIsShowPassword(!isShowPassword)} className="absolute cursor-pointer top-1/2 -translate-y-1/2 right-2 text-[#cfcfcf] lg:w-5 lg:h-5" />
            )}
          </div>
          <div className="flex items-center gap-2 mt-6">
            <input type="checkbox" id="register-tnc" required className=" accent-[#29A867] w-5 h-5 lg:w-6 lg:h-6" />
            <label className="text-[10px] sm:text-xs" htmlFor="register-tnc">
              Dengan mencentang ini, kamu menyetujui <span className="text-[#29A867]">Syarat & Ketentuan</span> kami.
            </label>
          </div>
          <div className="flex flex-col mt-10 lg:mt-4">
            <button disabled={isLoading} className="flex justify-center h-11 py-3 mb-3 bg-[#4DB680] text-sm text-white rounded-lg sm:static sm:w-full disabled:bg-[#cfcfcf]">
              {isLoading ? <Spinner className={"self-center border-l-black/20"} /> : "Daftar"}
            </button>
            <div className="text-xs text-center mb-2 sm:text-base">
              <span className="block sm:text-center sm:my-6">
                Sudah punya akun?{" "}
                <Link className="text-[#29A867] underline" to="/login">
                  Masuk
                </Link>
              </span>
            </div>
          </div>
        </form>
      </div>
      {errMsg && (
        <div className="absolute top-[120px] sm:top-32 xl:top-36 left-0 flex justify-center w-full text-xs xl:text-sm">
          <div className="px-3 py-1 rounded-xl text-center text-red-600 bg-red-100 border-red-600 border mb-2 font-medium capitalize">
            <p>{errMsg}</p>
          </div>
        </div>
      )}
    </div>
  );
}
