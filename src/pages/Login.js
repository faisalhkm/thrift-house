import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { loginSuccessAction } from "../action";
import { Link, useNavigate, Navigate } from "react-router-dom";
import axios from "axios";
import { InlineIcon } from "@iconify/react";
import Spinner from "../components/Spinner";

export default function Login() {
  const dispatch = useDispatch();
  const { access_token: accessToken } = useSelector((state) => state.login);
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [isShowPassword, setIsShowPassword] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [isError, setIsError] = useState(false);

  const baseUrl = "https://thrifthouse.herokuapp.com";

  const navigate = useNavigate();

  const handleSubmit = (e) => {
    e.preventDefault();
    setIsLoading(true);
    axios
      .post(`${baseUrl}/login`, {
        username,
        password,
      })
      .then((response) => {
        setIsLoading(false);
        if (response.data.hasOwnProperty("access_token")) {
          navigate("/");
          dispatch(loginSuccessAction(response.data));
        }
        setIsError(false);
      })
      .catch(() => {
        setIsError(true);
        setIsLoading(false);
        setTimeout(() => {
          setIsError(false);
        }, 3000);
      });
  };

  return accessToken ? (
    <Navigate to="/" replace />
  ) : (
    <div className="container my-9 px-5 sm:max-w-lg sm:mx-auto xl:flex xl:max-w-6xl xl:my-14 xl:gap-4">
      <img src="/images/login.png" alt="login" className="mx-auto w-40 sm:w-[420px] lg:w-[574px] object-contain" />
      <form className="flex flex-col justify-between h-full lg:justify-start" onSubmit={handleSubmit}>
        <div>
          <div className="text-center mt-6">
            <h1 className="font-bold text-[#29A867] sm:text-[42px]">Masuk ke akunmu</h1>
            <p className="text-slate-400 text-sm mt-1 mb-6 sm:text-lg">Masukkan data untuk melanjutkan</p>
          </div>
          <label className="" htmlFor="username">
            <span className="text-sm leading-5 mb-1 block">Username</span>
          </label>
          <input
            id="username"
            className="h-9 border-[#CFCFCF] border rounded-lg w-full text-xs py-3 px-2 mb-4 placeholder:text-slate-300 focus:outline-none focus:ring-1 block focus:ring-[#4DB680]"
            type={"text"}
            placeholder="Masukkan username kamu"
            required
            value={username}
            onChange={(e) => setUsername(e.target.value)}
          />
          <label className="" htmlFor="password">
            <span className="text-sm leading-5 mb-1 block">Kata Sandi</span>
          </label>
          <div className="relative">
            <input
              id="password"
              className="h-9 border-[#CFCFCF] border rounded-lg w-full text-xs py-3 px-2 placeholder:text-slate-300
          focus:outline-none focus:ring-1 block focus:ring-[#4DB680]"
              type={isShowPassword ? "text" : "password"}
              placeholder="Masukkan kata sandi kamu"
              required
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
            {isShowPassword ? (
              <InlineIcon icon="ri:eye-line" onClick={() => setIsShowPassword(!isShowPassword)} className="absolute cursor-pointer top-1/2 -translate-y-1/2 right-2 text-[#b6b6b6] lg:w-5 lg:h-5" />
            ) : (
              <InlineIcon icon="ri:eye-off-line" onClick={() => setIsShowPassword(!isShowPassword)} className="absolute cursor-pointer top-1/2 -translate-y-1/2 right-2 text-[#cfcfcf] lg:w-5 lg:h-5" />
            )}
          </div>
        </div>
        <div className="flex flex-col mt-10 lg:mt-0">
          <button disabled={isLoading} className="flex justify-center h-11 py-3 mb-3 bg-[#4DB680] text-sm text-white rounded-lg sm:static sm:w-full sm:mt-8 disabled:bg-[#cfcfcf]">
            {isLoading ? <Spinner className={"self-center border-l-black/20"} /> : "Masuk"}
          </button>
          <div className="text-xs text-center mb-2 left-1/2 sm:static sm:translate-x-0 sm:text-base">
            <span className="block sm:text-center sm:mt-5">
              Belum punya akun?{" "}
              <Link to="/register" className="text-[#29A867] underline">
                Daftar
              </Link>
            </span>
          </div>
        </div>
      </form>
      {isError && (
        <div className="absolute top-[120px] sm:top-32 xl:top-40 left-0 flex justify-center w-full text-xs xl:text-sm">
          <div className="px-3 py-1 rounded-xl text-center text-red-600 bg-red-100 border-red-600 border mb-2 font-medium capitalize">
            <p>Username / Password yang anda masukkan salah</p>
          </div>
        </div>
      )}
    </div>
  );
}
